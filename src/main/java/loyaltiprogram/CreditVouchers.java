package loyaltiprogram;

import java.util.*;

public class CreditVouchers {
    private HashMap<String, Integer> voucher = new HashMap<>();

    public HashMap<String, Integer> getVoucher() {
        return voucher;
    }

    public void setVoucher(HashMap<String, Integer> voucher) {
        this.voucher = voucher;
    }

    public void listVoucher() {
        HashMap<String, Integer> newVoucher = new HashMap<>();
        newVoucher.put("Rp. 10.000", 100);
        newVoucher.put("Rp. 25.000", 200);
        newVoucher.put("Rp. 50.000", 400);
        newVoucher.put("Rp. 100.000", 800);

        setVoucher(newVoucher);
    }

    public void getHighestPoint(){
        var map = getVoucher();
        Integer maxValue = Collections.max(map.values());

        for (Map.Entry<String, Integer> entry : map.entrySet()){
            if (Objects.equals(entry.getValue(), maxValue)){
                System.out.println("Maximum voucher is " + entry.getKey() + " with a point value of = " + entry.getValue() + "P");
            }
        }
    }

    public Integer redeemPointWithHighestPoint(int point){
        var map = getVoucher();
        Integer remainingPoint = null;

        Integer maxValue = Collections.max(map.values());

        for (Map.Entry<String, Integer> entry : map.entrySet()){
            if (Objects.equals(entry.getValue(), maxValue)){
                remainingPoint = point - entry.getValue();
            }
        }
        return remainingPoint;
    }

    public List<String> redeemAllPoints(int point){
        List<Integer> values = sortMap();

        List<Integer> pointsRedeemed = new ArrayList<>();
        List<String> voucher = new ArrayList<>();

        for (Integer value : values){
            if (point >= value){
                point = point - value;
                pointsRedeemed.add(value);
            }
        }

        var map = getVoucher();
        for (Integer o : pointsRedeemed) {
            for (Map.Entry<String, Integer> entry : map.entrySet()){
                if (Objects.equals(entry.getValue(), o)){
                    voucher.add(entry.getKey());
                }
            }
        }

        return voucher;
    }
    public Integer remainingPoint(int point) {
        List<Integer> values = sortMap();

        for (Integer value : values) {
            if (point >= value) {
                point = point - value;
            }
        }
        return point;
    }

    private List<Integer> sortMap(){
        var map = getVoucher();
        List<Integer> sortedMap = new ArrayList<>(map.values());
        sortedMap.sort(Collections.reverseOrder());

        return sortedMap;
    }
}
