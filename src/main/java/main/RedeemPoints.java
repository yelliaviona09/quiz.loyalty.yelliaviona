package main;

import loyaltiprogram.CreditVouchers;

import java.util.Scanner;

public class RedeemPoints {
    public static void main(String[] args) {
        CreditVouchers voucher = new CreditVouchers();
        voucher.listVoucher();

        Scanner in = new Scanner(System.in);
        System.out.print("Input your points : ");
        int v = in.nextInt();
        System.out.println("Your points is: "+ v);
        System.out.println("the voucher redeem process was successful : " + voucher.redeemAllPoints(v));
        System.out.println("your point remaining is : " + voucher.remainingPoint(v) + "p");
        in.close();
    }

}
